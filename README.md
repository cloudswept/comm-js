# @cloudswept/comm

Basic JavaScript communicator implementation for SQS

## `create`

#### Parameters 

- `options` - `object` - Options to create a communicator with
- `options.credentialsUrl` - `string` - The url to invoke to retrieve AWS credentials & context
- `options.publishUrl` - `string` - The url to invoke to publish
- `options.subscribeUrl` - `string` - The url to invoke to subscribe to a specific topic
- `options.getHeaders` - `Function` - A function which provides headers while invoking requests
- `options.authenticationEmitter` - [`Emitter`](https://github.com/developit/mitt) - An emitter which should emit an `authenticated` event when authenticated, and a `unauthenticated` event when unauthenticated, this will stop & start listening

#### Returns

- `Communicator` - Described below

## `Communicator`

### `listen`

#### Parameters

- `topic` - `string` - The topic to listen to 
- `onMessage` - `Function` - The function to invoke when there is an 
- `onError` - `Function` - The function to invoke when an error has occurred (Optional)
- `onCancel` - `Function` - The function to invoke when the subscription is completed being cancelled (Optional)

#### Returns

- `Promise.<Function>` - A promise resolving in a function which can be invoked to cancel listening

### `listenWithEmitter`

#### Parameters 

- `topic` - `string` - The topic to listen to

#### Returns

- `Promise.<ListenerEmitter>` - A promise resolving in an [emitter](https://github.com/developit/mitt) with an additional `cancel` function which can be invoked to cancel the listener

### `publish`

#### Parameters

- `topic` - `string` - The topic to publish to
- `payload` - `string` - The payload to publish to 

#### Returns

- `Promise` - A promise which resolves once the message has been published