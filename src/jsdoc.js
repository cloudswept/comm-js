/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
/**
 * @typedef {object} CommContext
 * @property {string} credentialsUrl
 * @property {string} publishUrl
 * @property {string} subscribeUrl
 * @property {Function} getHeaders
 * @property {Emitter|object} authenticationEmitter
 */

/**
 * @typedef {object} InternalCommContext
 * @property {string} credentialsUrl
 * @property {string} publishUrl
 * @property {string} subscribeUrl
 * @property {Function} getHeaders
 * @property {string} credentials
 * @property {Emitter|object} authenticationEmitter
 */

/**
 * @typedef {object} Emitter
 * @property {Function} on
 * @property {Function} off
 * @property {Function} emit
 */

/**
 * @typedef {Emitter} ListenerEmitter
 * @property {Function} on
 * @property {Function} off
 * @property {Function} cancel
 */

/**
 * @typedef {object} AWSCredentialsResponse
 * @property {AWSCredentialsResponseContext} context - The context to use, the property name can be
 *                                                     anything, e.g. CloudSweptContext will be accepted
 * @property {AWSCredentialsResponseCredentials} Credentials
 */

/**
 * @typedef {object} AWSCredentialsResponseCredentials
 * @property {string} AccessKeyId - The AWS access ID
 * @property {string} SecretAccessKey - The AWS secret access key
 * @property {string} SessionToken - The session token for this AWS credentials session
 * @property {string|null} Expiration - ISO Date string for when the credentials expire and should be refreshed
 */

/**
 * @typedef {object} AWSCredentialsResponseContext
 * @property {string} zone - A zone indicator (Stripped to 4 characters)
 * @property {string} type - A type indicator (Stripped to 1 character)
 * @property {string} uuid - A uuid to use while creating the queue name (Stripped to 36 characters)
 * @property {string} region - What AWS region should be used (e.g. us-west-2)
 */

/**
 * @typedef {object} Communicator
 * @property {ListenFunction} listen
 * @property {ListenEmitterFunction} listenWithEmitter
 * @property {PublishFunction} publish
 */

/**
 * @callback ListenFunction
 * @param {string} topic
 * @param {Function} onMessage
 * @param {Function|null} [onError=null]
 * @param {Function|null} [onCancelled=null]
 * @returns {Promise.<Function>} The resulting function can be called to cancel the subscription
 */

/**
 * @callback ListenEmitterFunction
 * @param {string} topic
 * @returns {Promise<ListenerEmitter|Emitter>}
 */

/**
 * @callback PublishFunction
 * @param {string} topic
 * @param {string} payload
 * @returns {Promise}
 */