/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
/**
 *
 * @param {InternalCommContext} context
 * @param topicDetails
 * @param additionalTopic
 * @returns {Promise.<void>}
 */
export async function subscribeToTopic(context, topicDetails, additionalTopic) {
  const response = await fetch(
    context.subscribeUrl,
    {
      method: 'POST',
      headers: {
        ...context.getHeaders(),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        topic: additionalTopic,
        payload: topicDetails.topic
      })
    }
  );
  await response.json();
}