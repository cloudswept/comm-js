/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
import Emitter from 'mitt';

/**
 * @param listener
 * @param topic
 * @returns {Promise<ListenerEmitter|Emitter>}
 */
export async function listenEmitter(listener, topic) {
  const emitter = Emitter();

  emitter.cancel = await listener(
    topic,
    (payload) => {
      emitter.emit('payload', payload);
    },
    (error) => {
      emitter.emit('error', error);
    },
    () => {
      emitter.emit('closed', null);
    }
  );

  return emitter;
}