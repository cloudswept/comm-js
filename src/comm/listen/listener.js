/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
import { createConnection } from '../connection';
import { subscribeToTopic } from '../subscribe';

export function createListener(context) {
  let listeners = [],
    globalCancel = null,
    connection;

  const invokeListeners = (property, args, topic = null) => {
    listeners
      .filter(listener => !topic || listener.topics.indexOf(topic) > -1)
      .filter(listener => listener[property] instanceof Function)
      .forEach(listeners => listeners[property](...args));
  };

  const getJSON = (data) => {
    if (typeof data !== 'string') {
      return null;
    }
    try {
      return JSON.parse(data);
    } catch(e) {
      return null;
    }
  };

  const onGlobalMessage = (data) => {
    const info = getJSON(data);
    if (!info || typeof info.topic !== 'string') {
      return;
    }
    invokeListeners('onMessage', [info.payload], info.topic);
  };

  const onGlobalError = (error) => {
    invokeListeners('onError', [error]);
  };

  const onGlobalCancelled = () => {
    invokeListeners('onCancelled', []);
    listeners = [];
    connection = null;
  };

  const getConnection = async () => {
    if (connection) {
      return connection;
    }
    connection = createConnection(context, onGlobalMessage, onGlobalError, onGlobalCancelled);
    const { cancel, getTopicDetails } = await connection;
    globalCancel = cancel;
    return { cancel, getTopicDetails };
  };

  return async (topic, onMessage, onError = null, onCancelled = null) => {
    const listener = {
      topics: Array.isArray(topic) ? topic : [topic],
      onMessage,
      onError,
      onCancelled
    };
    listeners.push(listener);
    const { getTopicDetails } = await getConnection();
    const topicDetails = await getTopicDetails();
    await Promise.all(
      listener.topics
        .filter(value => typeof value === 'string')
        .map(value => subscribeToTopic(context, topicDetails, value))
    );
    return () => {
      // Find our listener and remove it
      const index = listeners.indexOf(listener);
      // If it doesn't exist, either our connection was completely cancelled, or we already removed it
      if (index === -1) {
        return;
      }
      listeners.splice(index, 1);
      // If there are no more listeners, and we have a connection, cancel it
      if (listeners.length === 0 && globalCancel) {
        globalCancel();
      }
    }
  };
}