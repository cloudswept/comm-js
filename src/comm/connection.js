/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
import { listen as listenSQS } from '../aws/sqs';
import UUID from 'uuid';

function getContextFromResponse(response) {
  const found = Object.keys(response)
    .find(key => {
      const value = response[key];
      return value.region && value.type && value.uuid;
    });
  if (found) {
    return response[found];
  }
  throw new Error('No context found on AWS credentials response');
}

/**
 * @param {InternalCommContext} context
 * @param {Function} onMessage
 * @param {Function|null} [onError=null]
 * @param {Function|null} [onCancelled=null]
 * @returns {Promise.<{cancel: Promise, topicDetails: {region, topic: string}}>}
 */
async function createConnectionForUser(context, onMessage, onError = null, onCancelled = null) {
  const response = await context.credentials.getResponse();

  const responseContext = getContextFromResponse(response);

  const zone = (responseContext.zone || 'comm').substr(0, 4);
  const typeIndicator = responseContext.type.substr(0, 1).toLowerCase();
  const identifier = responseContext.uuid.substr(0, 36).toLowerCase();
  const sessionIdentifier = UUID.v4();

  const topicDetails = {
    region: responseContext.region,
    topic: `${zone}-${typeIndicator}-${identifier}-${sessionIdentifier}`
  };
  const cancel = await listenSQS(context.credentials, topicDetails, onMessage, onError, onCancelled);
  return { cancel, topicDetails };
}

/**
 * @param {InternalCommContext} context
 * @param {Function} onMessage
 * @param {Function|null} [onError=null]
 * @param {Function|null} [onCancelled=null]
 * @returns {Promise.<{cancel: (function()), getTopicDetails: (function(): {region, topic: string})}>}
 */
export async function createConnection(context, onMessage, onError = null, onCancelled = null) {
  let listening = true;

  let ourOnCancelled = () => {
    // If we're still listening, we're just cancelling an old connection
    if (listening) {

      return;
    }
    onCancelled();
  };

  // Get our initial authenticated queue
  let { cancel, topicDetails } = await createConnectionForUser(context, onMessage, onError, ourOnCancelled);

  // Whenever the user logs out, or is unauthenticated, stop listening
  context.authenticationEmitter.on('unauthenticated', () => {
    if (cancel instanceof Function) {
      cancel();
      cancel = null;
    }
  });

  // Wait for our user to authenticate again, and then re-create our connection
  context.authenticationEmitter.on('authenticated', () => {
    // If they cancelled the subscription, ignore, we shouldn't need to worry about this
    // too much, its just more if everything happened at once
    if (!listening) {
      return;
    }
    // If there is still an active connection, kill it, we want a new one
    if (cancel instanceof Function) {
      cancel();
      cancel = null;
    }
    createConnectionForUser(context, onMessage, onError, ourOnCancelled)
      .then(({ cancel: updatedCancel, topicDetails: updatedTopicDetails }) => {
        // If another connection came alive at the same time, kill it, whatever one is last wins
        if (cancel instanceof Function) {
          cancel();
        }
        cancel = updatedCancel;

        // Update the topic details
        topicDetails = updatedTopicDetails;
      })
      // Log any errors, its bas
      .catch(error => onError(error))
      .then(() => {
        // If we are no longer listening, kill it
        // This may have happened while we were creating a new connection
        if (!listening && cancel instanceof Function) {
          cancel();
          cancel = null;
        }
      });
  });

  const finalCancel = () => {
    listening = false;
    if (cancel) {
      cancel();
      cancel = null;
    } else {
      ourOnCancelled();
    }
  };

  return {
    cancel: finalCancel,
    getTopicDetails: async () => topicDetails
  }
}