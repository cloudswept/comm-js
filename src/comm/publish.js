/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
/**
 *
 * @param {InternalCommContext} context
 * @param {string} topic
 * @param {string} payload
 * @returns {Promise.<*>}
 */
export async function publish(context, topic, payload) {
  const response = await fetch(
    context.publishUrl,
    {
      method: 'POST',
      headers: {
        ...context.getHeaders(),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        topic,
        payload
      })
    }
  );
  return response.json();
}