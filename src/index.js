/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
import { getAWSCredentialsResponse } from './aws/remote-credentials-provider';
import { AWSRefreshingCredentials } from './aws/refreshing-credentials';
import { createListener } from './comm/listen/listener';
import { listenEmitter as listenEmitterFn } from './comm/listen/listen';
import { publish as publishFn } from './comm/publish';

/**
 *
 * @param {CommContext} options
 * @returns {Communicator}
 */
export function create(options) {
  const credentials = new AWSRefreshingCredentials(
    () => getAWSCredentialsResponse(options.credentialsUrl, options.getHeaders)
  );
  /**
   * @type {InternalCommContext|{credentials: AWSRefreshingCredentials}}
   */
  const extendedOptions = {
    ...options,
    credentials
  };

  const listener = createListener(extendedOptions);

  return {
    listen: async (topic, onMessage, onError = null, onCancelled = null) => {
      return listener(topic, onMessage, onError, onCancelled);
    },
    listenWithEmitter: async topic => {
      return listenEmitterFn(listener, topic);
    },
    publish: async (topic, payload) => {
      return publishFn(extendedOptions, topic, payload);
    }
  };
}