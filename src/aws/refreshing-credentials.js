/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
export class AWSRefreshingCredentials {

  constructor(generateCredentials) {
    this.generateCredentials = generateCredentials;

    // Use Object.defineProperty here as `super` re-defines secretAccessKey as non enumerable, however we want the
    // property to be a `get` function rather than getter/setter

    Object.defineProperty(
      this,
      'accessKeyId',
      {
        enumerable: true,
        get: () => this.response && this.response.Credentials && this.response.Credentials.AccessKeyId
      }
    );
    Object.defineProperty(
      this,
      'expired',
      {
        enumerable: true,
        get: () => this.needsRefresh()
      }
    );
    Object.defineProperty(
      this,
      'expireTime',
      {
        enumerable: true,
        get: () => {
          if (!(this.response && this.response.Credentials && this.response.Credentials.Expiration)) {
            return null;
          }
          const expirationDate = new Date(this.response.Credentials.Expiration);
          if (isNaN(expirationDate.getTime())) {
            return null;
          }
          return expirationDate;
        }
      }
    );
    Object.defineProperty(
      this,
      'secretAccessKey',
      {
        get: () => this.response && this.response.Credentials && this.response.Credentials.SecretAccessKey
      }
    );
    Object.defineProperty(
      this,
      'sessionToken',
      {
        enumerable: true,
        get: () => this.response && this.response.Credentials && this.response.Credentials.SessionToken
      }
    );


  }

  /**
   * @param {Function} callback
   */
  get(callback) {
    this.getPromise()
      .then(
        result => callback(null, result),
        error => callback(error, null)
      );
  }

  /**
   * @returns {Promise.<AWSCredentialsResponse>}
   */
  async getResponse() {
    if (this.needsRefresh()) {
      await this.refreshPromise();
    }
    return this.response;
  }

  /**
   * @returns {Promise.<*>}
   */
  async getPromise() {
    if (this.needsRefresh()) {
      return this.refreshPromise();
    }
    return this;
  }

  /**
   * @returns {boolean}
   */
  needsRefresh() {
    const expireTime = this.expireTime;
    if (!expireTime) {
      return true;
    }
    return expireTime.getTime() < Date.now();
  }

  /**
   * @param {Function} callback
   */
  refresh(callback) {
    this.refreshPromise()
      .then(
        result => callback(null, result),
        error => callback(error, null)
      );
  }

  /**
   * @returns {Promise.<*>}
   */
  async refreshPromise() {
    if (!(this.generateCredentials instanceof Function)) {
      throw new Error('generateCredentials is expected to be a function');
    }

    this.response = await this.generateCredentials();
  }

}