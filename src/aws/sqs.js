/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
import * as AWS from 'aws-sdk';

async function handleReceiveMessage(sqs, queueUrl, message, onMessage) {
  // Grab this early in case they want to mutate the message for whatever reason
  const receiptHandle = message.ReceiptHandle;
  let body = message.Body;
  if (message.MessageAttributes && message.MessageAttributes['Content-Type'] === 'application/json') {
    body = JSON.parse(body);
  }
  // console.log('Communicator >>> ' + body);
  const result = await onMessage(body);
  if (result === false) {
    return null; // Don't delete it from the queue
  }
  await new Promise((resolve, reject) => {
    sqs.deleteMessage(
      {
        QueueUrl: queueUrl,
        ReceiptHandle: receiptHandle
      },
      (error) => error ? reject(error) : resolve()
    )
  });
}

async function handleReceiveMessages(sqs, queueUrl, result, onMessage, handleError) {
  if (!Array.isArray(result.Messages)) {
    return null; // No message array is provided if no messages are available
  }

  await Promise.all(
    result.Messages
      .map(
        async message => {
          try {
            await handleReceiveMessage(sqs, queueUrl, message, onMessage);
          } catch(error) {
            handleError(error);
          }
        }
      )
  );

}

function receiveMessages(sqs, queueUrl, onMessage, onError, onCancelled) {
  if (!(onMessage instanceof Function)) {
    throw new Error('onMessage must be a function');
  }

  const handleError = (error) => {
    console.warn(error);
    if (onError instanceof Function) {
      onError(error);
    }
  };

  const parameters = {
    QueueUrl: queueUrl,
    WaitTimeSeconds: 20,
    // Give us 30 seconds locally to handle the message
    VisibilityTimeout: 30,
    MaxNumberOfMessages: 10
  };

  let cancelled = false;

  const getRetryDelayFromError = (error) => {
    const defaultRetry = 500;
    if (typeof error.retryDelay === 'number' && error.retryDelay > defaultRetry) {
      return error.retryDelay;
    }
    return defaultRetry;
  };

  const next = () => {
    if (cancelled) {
      if (onCancelled instanceof Function) {
        onCancelled();
      }
      return;
    }

    sqs.receiveMessage(
      parameters,
      (error, data) => {
        if (error) {
          handleError(error);
          // Step back if an error occurred, but still process
          setTimeout(next, getRetryDelayFromError(error));
        } else if (data) {
          handleReceiveMessages(sqs, queueUrl, data, onMessage, handleError)
            .catch(error => handleError(error))
            .then(next);
        }
      }
    );

  };

  next();

  return () => {
    cancelled = true;
  };
}

async function getQueueUrl(sqs, queue) {
  return new Promise(resolve => {
    sqs.getQueueUrl(
      {
        QueueName: queue
      },
      (error, result) => error ? resolve(false) : resolve(result)
    );
  });
}

async function ensureExistsAndGetQueueUrl(sqs, queue) {
  const getQueueUrlResult = await getQueueUrl(sqs, queue);
  if (getQueueUrlResult) {
    return getQueueUrlResult;
  }
  const options = {
    QueueName: queue,
    Attributes: {}
  };
  if (/\.fifo$/.test(queue)) {
    options.Attributes.FifoQueue = 'true';
    // We are using MessageDeduplicationId
    options.Attributes.ContentBasedDeduplication = false;
  }
  return new Promise((resolve, reject) => {
    sqs.createQueue(
      options,
      (error, result) => error ? reject(error) : resolve(result)
    );
  });
}

const getSQSForRegion = (() => {
  const cache = {};

  return (credentials, region) => {
    const options = {
      credentials,
      version: 'latest',
      region: region
    };
    if (cache[options.region]) {
      return cache[options.region];
    }
    const sqs = new AWS.SQS(options);
    cache[options.region] = sqs;
    return sqs;
  };
})();

export async function listen(credentials, topicDetails, onMessage, onError = null, onCancelled = null) {
  let region = null;
  let topicString = topicDetails;

  // Topic details could contain a region, so match if provided
  if (topicDetails.topic) {
    topicString = topicDetails.topic;
    region = topicDetails.region;
  }

  const sqs = getSQSForRegion(credentials, region);

  // Trigger an error if no credentials
  await credentials.getPromise();

  // Get the queue url or create it...
  const queueUrlResult = await ensureExistsAndGetQueueUrl(sqs, topicString);

  return receiveMessages(sqs, queueUrlResult.QueueUrl, onMessage, onError, onCancelled);
}