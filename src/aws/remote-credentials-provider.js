/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
import Storage from '../storage';

let storage = new Storage('aws-credentials');

/**
 * @returns {AWSCredentialsResponse|null}
 */
function getFromStorage() {
  try {
    const response = storage.getItem('response');
    if (typeof response !== 'string') {
      return null;
    }
    const json = JSON.parse(response);
    if (!json.Credentials) {
      return null;
    }
    const expiration = json.Credentials.Expiration;
    if (typeof expiry !== 'string') {
      return null;
    }
    const expirationDate = new Date(expiration);
    if (isNaN(expirationDate.getTime())) {
      return null;
    }
    if (expirationDate.getTime() < Date.now()) {
      return null;
    }
    return json;
  } catch(e) {
    return null;
  }
}

function addToStorage(value) {
  storage.setItem('response', JSON.stringify(value));
}

/**
 * @param {string} url
 * @param {Function} getHeaders
 * @returns {Promise.<AWSCredentialsResponse>|AWSCredentialsResponse}
 */
export async function getAWSCredentialsResponse(url, getHeaders) {
  const cached = getFromStorage();
  if (cached) {
    return cached;
  }
  const response = await fetch(
    url,
    {
      headers: {
        ...getHeaders()
      }
    }
  );
  const json = await response.json();
  addToStorage(json);
  return json;
}

/**
 * @typedef {object} AWSCredentialsResponse
 * @property {AWSCredentials} Credentials
 * @property {string} DeviceTopic
 */

/**
 * @typedef {object} AWSCredentials
 * @property {string} AccessKeyId
 * @property {string} SecretAccessKey
 * @property {string} SessionToken
 * @property {string} Expiration
 */