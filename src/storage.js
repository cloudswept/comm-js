/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
import StorageBase from 'local-storage-fallback';

const PREFIX = Symbol('Prefix');

function getKey(storage, key) {
  if (!storage[PREFIX]) {
    return key;
  }
  return `${storage[PREFIX]}:${key}`
}

class Storage {

  constructor(prefix) {
    this[PREFIX] = prefix;
  }

  getItem(key) {
    return StorageBase.getItem(getKey(this, key));
  }

  setItem(key, value) {
    return StorageBase.setItem(getKey(this, key), value);
  }

  removeItem(key) {
    return StorageBase.removeItem(getKey(this, key));
  }

  static getDefaultStorage() {
    if (Storage.default) {
      return Storage.default;
    }
    Storage.default = new Storage('cloudswept-comm');
    return Storage.default;
  }

  static getItem(key) {
    return Storage.getDefaultStorage().getItem(key);
  }

  static setItem(key, value) {
    return Storage.getDefaultStorage().setItem(key, value);
  }

  static removeItem(key) {
    return Storage.getDefaultStorage().removeItem(key);
  }

  static clear() {
    return StorageBase.clear();
  }

}

export default Storage;